<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage kunev
 *
 */

?>
<?php wp_footer(); ?>
		 <footer>	
		 	<!-- Yandex.Metrika counter -->
			<script type="text/javascript">
			(function (d, w, c) {
			    (w[c] = w[c] || []).push(function() {
			        try {
			            w.yaCounter29270795 = new Ya.Metrika({id:29270795,
			                    webvisor:true,
			                    clickmap:true,
			                    trackLinks:true,
			                    accurateTrackBounce:true});
			        } catch(e) { }
			    });

			    var n = d.getElementsByTagName("script")[0],
			        s = d.createElement("script"),
			        f = function () { n.parentNode.insertBefore(s, n); };
			    s.type = "text/javascript";
			    s.async = true;
			    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			    if (w.opera == "[object Opera]") {
			        d.addEventListener("DOMContentLoaded", f, false);
			    } else { f(); }
			})(document, window, "yandex_metrika_callbacks");
			</script>
			<noscript><div><img src="//mc.yandex.ru/watch/29270795" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
			<!-- /Yandex.Metrika counter --> 	
            <div class="container">
            	<div class="row">
            		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h3>Кухня по индивидуальному проекту<br>за 1 шаг</h3>                    
		                <!--<button class="btn" data-toggle="modal" data-target="#designer">Заказать БЕСПЛАТНЫЙ выезд</button>-->
	                </div>
                    <div class="copyright col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <p><span>©</span>  2012-2017 «Кухнев»</p>
                    </div>
    
	                <div class="footer-phone col-lg-6 col-md-6 col-sm-6 col-xs-12">
	                    <p class="ya-phone">8-495-989-44-15</p>
	                </div>
                </div>
            </div>
		 </footer>	
		 <?php get_template_part('tpl/modals'); ?> 
	</body>		
</html>