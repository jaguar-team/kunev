<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(); ?></title>
        <script type="text/javascript">
            var __cs = __cs || [];
            __cs.push(["setCsAccount", "REXY3PUfcTw4g9AaNf7eVLscbQ5Qs1ui"]);
            __cs.push(["setCsHost", "//server.comagic.ru/comagic"]);
        </script>
        <script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
        <?php wp_head(); ?>
    </head>
    
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="left col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center-sm">
                        <p>Красивые и функциональные кухни</p>
                        <p>ПО ЕВРОПЕЙСКИМ СТАНДАРТАМ</p>
                        <p>КАЧЕСТВА</p>
                    </div>                    
                    <div class="right col-lg-4 col-lg-push-4 col-md-4 col-md-push-4 col-sm-6 col-xs-12 text-center-sm">
                        <a class="comagic_phone" href="tel:8-495-989-44-15">8-495-989-44-15</a><br>
                        <button class="btn" data-toggle="modal" data-target="#call">Заказать обратный звонок</button>
                    </div>
                    <div class="logo col-lg-4 col-lg-pull-4 col-md-4 col-md-pull-4 col-sm-12 col-xs-12">
                        <img src="<?= get_template_directory_uri().'/images/logo.png';?>" alt="" />
                    </div>
                </div>
            </div>
        </header>
        <?php $img_banner = get_option('banner_img');?>
        <?php $info_banner = get_option('banner_option');?>
        <section class="banner" style="background:url(<?=$img_banner;?>) no-repeat center bottom; background-size: cover;">
            <div class="container">
                <div class="main-header">
                    <div class="back"></div>
                    <div class="content">
                        <div class="top"><?=$info_banner['header_top'];?></div>
                        <div class="discount"><?=$info_banner['header_center'];?></div>
                        <div class="bot"><?=$info_banner['header_bot'];?></div>
                    </div>
                </div>
                <div class="features">
                    <div class="block">
                        <div class="img">
                            <img src="<?= get_template_directory_uri().'/images/bg/clip/clip1.png';?>" alt="" />
                        </div>
                        <p><span>Бесплатный</span><br/>
                            выезд
                        </p>
                    </div>
                    <div class="block">
                        <div class="img">
                            <img src="<?= get_template_directory_uri().'/images/bg/clip/clip2.png';?>" alt="" />
                        </div>
                        <p>Минимальный срок<br/>
                            <span>службы 15 лет</span>
                        </p>
                    </div>
                    <div class="block">
                        <div class="img">
                            <img src="<?= get_template_directory_uri().'/images/bg/clip/clip3.png';?>" alt="" />
                        </div>
                        <p>Возможность покупки<br/>
                            <span> в рассрочку</span>
                        </p>
                    </div>
                    <div class="block">
                        <div class="img">
                            <img src="<?= get_template_directory_uri().'/images/bg/clip/clip4.png';?>" alt="" />
                        </div>
                        <p>Установка<br/>
                            <span>за 1 день</span>
                        </p>
                    </div>
                    <div class="block">
                        <div class="img">
                            <img src="<?= get_template_directory_uri().'/images/bg/clip/clip5.png';?>" alt="" />
                        </div>
                        <p><span>Бесплатная</span><br/>
                            доставка                                
                        </p>
                    </div>
                </div>
            </div>
        </section> 

        <nav class="menu">
            <div class="container">                
                <div class="logo">
                    <img src="<?= get_template_directory_uri().'/images/logo2.png';?>" alt="" />
                </div>
                <div class="menu-container hidden-sm hidden-xs max">
                    <ul>                            
                        <li>
                            <a href="#our">Каталог кухонь</a>
                        </li>
                        <li>
                            <a href="#steps">Наши преимущества</a>
                        </li>
                        <li>
                            <a href="#facades">Каталог фасадов</a>
                        </li>
                        <li>
                            <a href="#way">Наш подход</a>
                        </li>
                        <li>
                            <a href="#variants">Планировка кухни</a>
                        </li>
                        <li>
                            <a href="#service">Выездной дизайнер</a>
                        </li>
                        <li>
                            <a href="#inside">Кухня изнутри</a>
                        </li> 
                        <li>
                            <a href="#producers">Бытовая техника</a>
                        </li>
                        <!--<li>
                            <a href="#time">Акции</a>
                        </li>-->
                        <li>
                            <a href="#callback">Отзывы</a>
                        </li>
                        <li>
                            <a href="#map">Контакты</a>
                        </li>
                    </ul>
                </div>   
                <div class="phone" data-toggle="modal" data-target="#call"><i class="fa fa-phone"></i></div>
                <div class="menu-min dropdown hidden-lg hidden-md">
                    <i class="fa fa-bars dropdown-toggle" data-toggle="dropdown"></i>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#our">Каталог кухонь</a>
                        </li>
                        <li>
                            <a href="#steps">Наши преимущества</a>
                        </li>
                        <li>
                            <a href="#facades">Каталог фасадов</a>
                        </li>
                        <li>
                            <a href="#way">Наш подход</a>
                        </li>
                        <li>
                            <a href="#variants">Планировка кухни</a>
                        </li>
                        <li>
                            <a href="#service">Выездной дизайнер</a>
                        </li>
                        <li>
                            <a href="#inside">Кухня изнутри</a>
                        </li> 
                        <li>
                            <a href="#producers">Бытовая техника</a>
                        </li>
                        <li>
                            <a href="#time">Акции</a>
                        </li>
                        <li>
                            <a href="#callback">Отзывы</a>
                        </li>
                        <li>
                            <a href="#map">Контакты</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>   