<?php 
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage g-r
 */


/** send main **/

get_header(); ?>
<?php $background = get_option('img_background'); ?>
<?php $background_clarify = get_option('banner_img_block_cost'); ?>
<?php sendUploadPhotoBlock(); ?>
<?php sendFreeProject(); ?>
<?php sendEskiz(); ?>
<section class="our" id="our" style="background: url(<?=$background;?>);">
	<?php get_template_part('tpl/our'); ?>
</section>

<section class="clarify" id="clarify" style="background:url(<?=$background_clarify;?>) no-repeat center bottom; background-size: cover;">
	<?php get_template_part('tpl/clarify'); ?>
</section>

<section class="steps" id="steps">
	<?php get_template_part('tpl/steps'); ?>
</section>

<section class="facades" id="facades" style="background: url(<?=$background;?>);">
	<?php get_template_part('tpl/facades'); ?>
</section>


<section class="way" id="way">
	<?php get_template_part('tpl/way'); ?>
</section>

<section class="variants" id="variants" style="background: url(<?=$background;?>);">
	<?php get_template_part('tpl/variants'); ?>
</section>

<section class="service" id="service">
	<?php get_template_part('tpl/service'); ?>
</section>

<section class="inside" id="inside">
	<?php get_template_part('tpl/inside'); ?>
</section>

<!--<section class="price">
	
</section>-->

<section class="producers" id="producers">
	<?php get_template_part('tpl/producers'); ?>
</section>

<section class="time" id="time">
	<?php get_template_part('tpl/time'); ?>
</section>

<section class="callback" id="callback">
	<?php get_template_part('tpl/callback'); ?>
</section>

<section class="find" id="map">
	<?php get_template_part('tpl/find'); ?>
</section>

<?php get_footer(); ?>    

					