jQuery(document).ready(function($) {
	
	function Menu(){
		var top = $(document).scrollTop();
		if (top < 800) {
			$(".menu").removeClass('float');							
		}				
		else {
			$(".menu").addClass('float');						
		}
	}

	function comagicSend(data){
		Comagic.addOfflineRequest({
			name: data.name,
			phone: data.phone_number,
			message: data.message + '(Заявка с формы:"' + data.subject + '")'
		}, function(res){
			//console.log(data);
			var status = JSON.parse(res.response).success;
            if (status) {                        
                afValidated = true;
            }
            else {
                afValidated = false;
            }
		});
	}

	function disableArrows(el){
   		el.children('.slider-arrows').removeClass('disabled');

        if (el.find('.carousel-inner .item:last-child').hasClass('active')){
           
            el.children('.slider-arrows.next').addClass('disabled');
        }
        if (el.find('.carousel-inner .item:first-child').hasClass('active')){

            el.children('.slider-arrows.prev').addClass('disabled');
        }
        if(!el.find('.carousel-inner .item').length){

        	el.children('.slider-arrows').addClass('disabled');
        }
   	}

	Menu();
	var files;
	$('.file-upload').click(function(){
		$(this).next('input[type="file"]').click();
	});
	$('input[type="file"]').each(function(){
		$(this).change(function(){
	    files = this.files;
	});
	var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

   	$('input[type="file"]').each(function(){
   		$(this).change(function(){
        var file_name;
        if( file_api && $(this)[ 0 ].files[ 0 ] )
            file_name = $(this)[ 0 ].files[ 0 ].name;
        else
            file_name = $(this).val().replace( "C:\\fakepath\\", '' );

        if( ! file_name.length )
            return;

            $(this).prev().text(file_name);
            $(this).prev().css('color: #9A9485');
    	}).change();
   	});


	});
	$(window).scroll(function(){
		Menu();
		$('section').each(function(){
			if ($(document).scrollTop() - $(".menu.float").height() > $(this).offset().top && $(document).scrollTop() - $(this).offset().top < $(this).height()){
				$('.menu a[href*="' + $(this).attr('id') + '"]').addClass('active');
			}
			else{
			    $('.menu a[href*="' + $(this).attr('id') + '"]').removeClass('active');
			}
		});		
	});
	
	$('#call form').on('submit', function(e){
		e.preventDefault();
		var data = {
			action: 'send_mail',
			subject: 'Обратный звонок',
			name: $(this).find('input.name').val(),
			phone_number: $(this).find('input.phone').val(),
			message: $(this).find('textarea.comment').val()
		};
		if(!$(this).children().hasClass('disabled') || !$(this).children().hasClass('untouched')){
			$.post(send_mail_kunev.url, data, function(res){
				//console.log(res);
			});
			$('#call').modal('hide');
			$(this).find("input[type=text], textarea").val("");
			comagicSend(data);
			$('#success').modal('show');
		}
	});


	$('#designer form').on('submit', function(e){
		e.preventDefault();
		var data = {
			action: 'send_mail',
			subject: 'Вызов дизайнера',
			name: $(this).find('input.name').val(),
			phone_number: $(this).find('input.phone').val(),
			message: $(this).find('textarea.comment').val()
		};
		if(!$(this).children().hasClass('disabled') || !$(this).children().hasClass('untouched')){
			$.post(send_mail_kunev.url, data, function(res){
				//console.log(res);
			});
			$('#designer').modal('hide');
			$(this).find("input[type='text'], textarea").val("");
			comagicSend(data);
			$('#success').modal('show');
		}
	});

	/**$('#photo form').on('submit', function(e){
		e.preventDefault();
		if($(this).children().hasClass('disabled') || $(this).children().hasClass('untouched')){
			e.preventDefault();
		}
	});**/

	$('#image').on('show.bs.modal', function (event) {
		var block = $(event.relatedTarget);
		var data = block.find('img').attr('src');
		$(this).find('.modal-content').append('<img src="' + data + '" class="popup-img">');
	});

	$('#image').on('hide.bs.modal', function (event) {
		$(this).find('.modal-content').children().remove();
	});

	$('#photo').on('show.bs.modal', function (event) {
		$(this).find('.file-upload').text('Выбрать файл').css({'color': '#a9a9b'});
	});

	$('#photos').on('show.bs.modal', function (event) {
		$(this).find('.file-upload').text('Выбрать файл').css({'color': '#a9a9b'});
	});

	$('#free').on('show.bs.modal', function (event) {
		$(this).find('.file-upload').text('Выбрать файл').css({'color': '#a9a9b'});
	});
	
	$('#kitchen').on('show.bs.modal', function (event) {
		var $this = $(this);
		var active = $(event.relatedTarget).attr('data-number');
		var data = {
			action: 'get_kitchen_by_slug',
			slug: $(event.relatedTarget).attr('data-slider'),			
		}
		var content = ``;
		var startContent = 
			`
				<div id="kitchen-carousel` + data.slug + `" class="carousel slide" data-ride="carousel" data-wrap="false">
                	<div class="carousel-inner" role="listbox">
			`;
		var endContent = 
			`
				</div>

                <a class="slider-arrows next" href="#kitchen-carousel` + data.slug + `" role="button" data-slide="next"></a>
                <a class="slider-arrows prev" href="#kitchen-carousel` + data.slug + `" role="button" data-slide="prev"></a>
            </div>
			`;
		
		
		$.post( get_kitchen.url, data, function(resp){
			//console.log(resp);
			for (var i = 0; i < resp.length; i++) {
				content += 
					`
					
							<div class="item ` + (i == active ? "active" : "") + `">
									<div class="left">
										<h3>` + resp[i].title + `</h3>
										<p class="old-price">` + resp[i].coast + ` <span class="fa fa-rub"></span></p>
										<p class="new-price">` + resp[i].new_coast + ` <span class="fa fa-rub"></span></p>
										<ul>
						    				<li>
						    					<span class="title">Фасад</span>
						    					<span>` + resp[i].fasad + `</span>
						    				</li>
						    				<li>
						    					<span class="title">Каркас</span>
						    					<span>` + resp[i].karkas + `</span>
						    				</li>
						    				<li>
						    					<span class="title">Столешница</span>
						    					<span>` + resp[i].desk + `</span>
						    				</li>
						    				<li>
						    					<span class="title">Фурнитура</span>
						    					<span>` + resp[i].furnitura + `</span>
						    				</li>
						    			</ul>
						    			<button class="btn" data-toggle="modal" data-target="#call">Получить консультацию</button>
										<button class="btn" data-toggle="modal" data-target="#free">Бесплатный проект</button>
									</div>
									<div class="img">
										<img src="` + resp[i].img[0] + `">
									</div>
							</div>
						
					`
			}
			$this.find('.inner').html(startContent + content + endContent);
			disableArrows($this.find('.carousel'));
			$this.find('.carousel').on('slid.bs.carousel', function(){
				disableArrows($(this));
			});
		});
	});

	$('.our .item .btn').click(function(e){
		$($(this).attr('data-target')).modal('show');
		console.log('qweqwew');
			e.stopPropagation();
	});

	$('.menu a[href^="#"]').click(function(){		
        var el = $(this).attr('href');
        $('body').animate({
            scrollTop: $(el).offset().top - $(".menu.float").height()}, 700);
        return false; 
	});

	$('.modal-dialog').click(function(event){		
		if (event.target == this){
			$(this).parent().modal('hide');
		}
		else return;		
	});

	
	$('.phone').inputmask({
		mask: '8(999)999-99-99',		 
		"onincomplete": function(){ 
			$(this).css('border-color', '#d00000')
			.parent().find('input[type="submit"]').addClass('disabled');
		},		
		"oncomplete": function(){
			$(this).css('border-color', '#9A9485')
			.parent().find('input[type="submit"]').removeClass('disabled');;
		},
		"onKeyDown": function(){
			$(this).removeClass('untouched');
		}
	});
	$('.name').inputmask({
		mask: '*{2,}',
		placeholder: '',
		"onincomplete": function(){ 
			$(this).css('border-color', '#d00000')
			.parent().find('input[type="submit"]').addClass('disabled');
		},		
		"oncomplete": function(){
			$(this).css('border-color', '#9A9485')
			.parent().find('input[type="submit"]').removeClass('disabled');
		},
		"onKeyDown": function(){
			$(this).removeClass('untouched')
			.parent().find('input[type="submit"]').removeClass('disabled');
		}
	});

	

	$('.carousel').on('slid.bs.carousel', function (e) {          

        disableArrows($(this));

    });

   $('.carousel').each(function(){
        
        disableArrows($(this));        
        
    });

	$('.carousel').bcSwipe({ threshold: 50 });
    
    $('.catalog .carousel').on('slide.bs.carousel', function(){
    	console.log($(this).find('.tab-item'));
    	$(this).find('.tab-item').removeClass('animated');
    });

    $('.catalog .nav-tabs a').on('show.bs.tab', function(e){
    	console.log($($(e.target).attr('href')).find('.tab-item'));
    	$($(e.target).attr('href')).find('.tab-item').addClass('animated');
    });

	$('.slider').slick({
		slidesToShow: 1,
	    slidesToScroll: 1,
	    autoplay: false,
	    autoplaySpeed: 4000,
	    dots: false,
	    infinite: false,
	    arrows: true,
	   	nextArrow: '<div class="slider-arrows next"></div>',
	   	prevArrow: '<div class="slider-arrows prev"></div>',
	    responsive: [
		        
    	]
	});
	$('.inside .labels').slick({
		slidesToShow: 7,
	    slidesToScroll: 7,
	    autoplay: false,
	    autoplaySpeed: 1500,
	    dots: false,
	    infinite: false,
	    arrows: false,
	    variableWidth: true,
	    responsive: [
		    {
		      breakpoint: 991,
		      settings: {
		        slidesToShow: 4,
	    		slidesToScroll: 1,
	    		autoplay: true,
	    		infinite: true,
		      }
		    },		    
    	]
	});

	
	$('.inside .slider-nav').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  dots: false,
	  autoplay: false,
	  //centerMode: true,
	  focusOnSelect: true,
	  arrows: false,
	  adaptiveHeight: true
	});

    
    
});