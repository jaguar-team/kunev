/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ymaps.ready(init);
function init() {
    var myMap = new ymaps.Map("map", {
        center: [55.747990, 37.592033],
        zoom: 16
    }),
            // Создаем метку с помощью вспомогательного класса.
            myPlacemark_1 = new ymaps.Placemark([55.746380, 37.592033], {}, {
                iconImageHref: 'wp-content/themes/kunev/images/bg/clip/clip31.png',
                iconImageSize: [135, 137],
                iconImageOffset: [-52, -137]
            });

    myMap.controls.add(
            new ymaps.control.ZoomControl()
            );


    // Добавляем все метки на карту.
    myMap.geoObjects.add(myPlacemark_1);
    myMap.behaviors
            .disable(['scrollZoom']);

}

