<div class="container">		
	<h3><span>Услуга</span>ВЫЕЗДНОЙ ДИЗАЙНЕР-конструктор<br><small>– это комплексный подход к вашей кухне:</small></h3>
	<div class="content">
		<div class="row">
            <ul class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <li>Замер помещения</li>
                <li>Консультации по дизайну кухни и выбору материалов</li>
                <li>Составление дизайн-проекта кухни в 3D</li>
                <li>Просчет стоимости с точностью до рубля</li>
            </ul>
            <ul class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-0 col-xs-12">
                <li>Подбор техники и аксессуаров</li>
                <li>Заключение договора и прием предоплаты</li>
                <li>Последующая координация доставки и сборки кухни</li>                            
            </ul>
        </div>
    </div>
    <div class="block-center">
        <button class="btn" data-toggle="modal" data-target="#designer">Заказать бесплатный выезд</button>
        <div class="content">
            <div class="block">
                <div class="img">
                    <img src="<?= get_template_directory_uri().'/images/bg/clip/clip19r.png';?>" alt="">
                </div>
                <div class="clear"></div>
                <h5>ВЫЕЗД В ЛЮБОЕ <br>УДОБНОЕ ВРЕМЯ</h5>
                <p>Никаких пробок и утомительных поездок по мебельным центрам</p>
            </div>
            <div class="block">
                <div class="img">
                    <img src="<?= get_template_directory_uri().'/images/bg/clip/clip20r.png';?>" alt="">
                </div>
                <div class="clear"></div>
                <h5>ОБРАЗЦЫ МАТЕРИАЛОВ<br>ВСЕГДА С СОБОЙ</h5>
                <p>Оцените качество фасадов в реальном размере</p>
            </div>
            <div class="block">
                <div class="img">
                    <img src="<?= get_template_directory_uri().'/images/bg/clip/clip21r.png';?>" alt="">
                </div>
                <div class="clear"></div>
                <h5>СОВЕТЫ ЭКСПЕРТА</h5>
                <p>Полезная консультация по оснащению кухни и выбору материалов</p>
            </div>
            <div class="block">
                <div class="img">
                    <img src="<?= get_template_directory_uri().'/images/bg/clip/clip22r.png';?>" alt="">
                </div>
                <div class="clear"></div>
                <h5>КОМЛЕКСНЫЙ<br>ПОДХОД</h5>
                <p>Эскиз, просчет и оформление договора за один визит</p>
            </div>
            <div class="block">
                <div class="img">
                    <img src="<?= get_template_directory_uri().'/images/bg/clip/clip23r.png';?>" alt="">
                </div>
                <div class="clear"></div>
                <h5>В РАМКАХ ВАШИХ <br>ВОЗМОЖНОСТЕЙ</h5>
                <p>Дизайнер-конструктор впишется в ваш бюджет без потери качества</p>
            </div>
            <div class="block">
                <div class="img">
                    <img src="<?= get_template_directory_uri().'/images/bg/clip/clip24r.png';?>" alt="">
                </div>
                <div class="clear"></div>
                <h5>РАБОТА НА МЕСТЕ</h5>
                <p>Профессиональный взгляд на помещение кухни</p>
            </div>
        </div>
    </div>
    <p class="subtext">УДОБСТВО, ЭКОНОМИЯ ВРЕМЕНИ И ПРОФЕССИОНАЛЬНЫЙ ПОДХОД К ВАШЕЙ КУХНЕ</p>
</div>