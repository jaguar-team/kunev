<div class="container">
	<h3><span>Более 500 фасадов</span></h3>
	<div class="catalog">
		<div class="row">
			
			<ul class="nav nav-tabs" role="tablist">
				<?php 
								$args = array(
						'taxonomy' => 'fasad-cat',
						'order'    => 'ask',
						'hide_empty' => false,
					);
					$fasad = get_terms( $args );
					$i = 0;
					
					foreach ($fasad as $value):?>
			    <li role="presentation" class="<?= ($i==0) ? 'active' : ''; ?>"><a href="#<?=$value->slug?>" aria-controls="<?=$value->slug?>" role="<?=$value->slug?>" data-toggle="tab">
			    	<?=$value->description?><br>
			    	<span><?=$value->name?></span>
			    </a></li> 
			    <?php $i++; endforeach; ?>
			</ul>
			
			<div class="tab-content">
				<?php $i = 0;
			  		  foreach ( $fasad as $value ):?>
				<div role="tabpanel" class="tab-pane  <?= ($i==0) ? 'active' : ''; ?>" id="<?=$value->slug?>">
					<div id="carousel<?=$value->slug ?>" class="carousel slide" data-ride="carousel" data-wrap="false" data-interval="false">
						<div class="carousel-inner" role="listbox">
						<?php $single_fasad = query_posts(array('fasad-cat' => $value->slug, 'post_type' => 'fasad','numberposts' => -1));?>
						<?php $j = 0;
								foreach ($single_fasad as $value_fasad):?>
						<?php $images = json_decode(get_post_meta( $value_fasad->ID, 'images', 1 )); ?>
						
							<div class="item <?= ($j==0) ? 'active' : ''; ?>">
							 	<div class="tab-item animated zoomInLeft img">
							 		<?php if($images[0]): ?>
										<img src="<?=$images[0] ?>" alt="">
									<?php endif; ?>
								</div>
								<div class="tab-item animated zoomInLeft zoomInUP img">
									<?php if($images[1]): ?>
										<img src="<?=$images[1] ?>" alt="">
									<?php endif; ?>
								</div> 
								<div class="tab-item animated zoomInLeft zoomInDown img">
									<?php if($images[2]): ?>
										<img src="<?=$images[2] ?>" alt="">
									<?php endif; ?>
								</div>
								<div class="tab-item animated zoomInLeft zoomInUP zoomInRight img">
									<?php if($images[3]): ?>
										<img src="<?=$images[3] ?>" alt="">
									<?php endif; ?>
								</div>
								<div class="tab-item animated zoomInLeft img">
									<?php if($images[4]): ?>
										<img src="<?=$images[4] ?>" alt="">
									<?php endif; ?>
								</div>
								<div class="tab-item animated zoomInLeft zoomInUP zoomInDown img">
									<?php if($images[5]): ?>
										<img src="<?=$images[5] ?>" alt="">
									<?php endif; ?>
								</div>
								<div class="tab-item animated zoomInLeft img">
									<?php if($images[6]): ?>
										<img src="<?=$images[6] ?>" alt="">
									<?php endif; ?>
								</div>
							</div>					
						<?php $j++; endforeach; ?>
						</div>
						<a class="slider-arrows next" href="#carousel<?=$value->slug?>" role="button" data-slide="next">
						    
						</a>

						<a class="slider-arrows prev" href="#carousel<?=$value->slug?>" role="button" data-slide="prev">
						    
						</a>
					</div>
					<div class="tab-line">
						<?php $harak = json_decode(get_term_meta( $value->term_id, 'product_harak', 1 ));
							if($harak) 
							foreach ($harak as $description): ?>					
		                    <div class="tab-line-item img">
		                        <img src="<?= $description->value;?>" alt="">
		                        <p><?=$description->name?></p>
		                    </div>
		                <?php endforeach;?>
	                </div>
				</div>
				<?php $i++; endforeach;?>
			</div>
		</div>		
	</div>
</div>