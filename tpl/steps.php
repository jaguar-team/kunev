<div class="container">
		<div class="block1-inner hidden-sm hidden-xs">
            <div class="block1-item">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip6y.png';?>" alt="">
                <p>Кухни от 40 
                    до 500 <br>тыс.руб.</p>
            </div>
            <div class="block1-item">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip13y.png';?>" alt="">
                <p>ЛЮБЫЕ НЕСТАНДАРТНЫЕ РАЗМЕРЫ И РЕШЕНИЯ

                </p>
            </div>
            <div class="block1-item">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip7y.png';?>" alt="">
                <p>ВЛАГОСТОЙКИЕ
                    <br>столешницы</p>
            </div>
            <div class="block1-item">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip8y.png';?>" alt="">
                <p>ЕВРОПЕЙСКИЕ материалы<br>
                    и комплектующие</p>
            </div>
            <div class="block1-item">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip9y.png';?>" alt="">
                <p>Более 500 фасадов<br>
                    для реализации
                    ВАШИХ ИДЕЙ</p>
            </div>
            <div class="block1-item">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip10y.png';?>" alt="">
                <p>Безопасность
                    <br>ДЛЯ ЗДОРОВЬЯ</p>
            </div>
            <div class="block1-item">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip14y.png';?>" alt="">
                <p>БЕСПЛАТНАЯ доставка <br>
                    и подъем в квартиру</p>
            </div>
            <div class="block1-item">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip12y.png';?>" alt="">
                <p>бесплатный 
                    <br>надежный монтаж</p>
            </div>
            <div class="center-block">                           
                <h3>ПОЧЕМУ<br>
                    <span>НАШИ КУХНИ</span><br> 
                    ЛУЧШЕ?</h3>
            </div>
        </div>
        <div class="min visible-sm visible-xs">
        	<h3>ПОЧЕМУ
                <span>НАШИ КУХНИ</span> 
                ЛУЧШЕ?
            </h3>
            <ul>
            	<li>
            		<span class="num">1</span>
            		<img src="<?= get_template_directory_uri().'/images/bg/clip/clip6y.png';?>" alt="">
	                <span>Кухни от 40 до 500 тыс.руб.</span>
            	</li>
            	<li>
            		<span class="num">2</span>
            		<img src="<?= get_template_directory_uri().'/images/bg/clip/clip13y.png';?>" alt="">
	                <span>Любые нестандартные размеры и решения</span>
            	</li>
            	<li>
            		<span class="num">3</span>
            		<img src="<?= get_template_directory_uri().'/images/bg/clip/clip7y.png';?>" alt="">
	                <span>Влагостойкие столешницы</span>
            	</li>
            	<li>
            		<span class="num">4</span>
            		<img src="<?= get_template_directory_uri().'/images/bg/clip/clip8y.png';?>" alt="">
	                <span>Европейские материалы и комплектующие</span>
            	</li>
            	<li>
            		<span class="num">5</span>
            		<img src="<?= get_template_directory_uri().'/images/bg/clip/clip9y.png';?>" alt="">
	                <span>Более 500 фасадов для реализации Ваших идей</span>
            	</li>
            	<li>
            		<span class="num">6</span>
            		<img src="<?= get_template_directory_uri().'/images/bg/clip/clip10y.png';?>" alt="">
	                <span>Безопасность для здоровья</span>
            	</li>
            	<li>
            		<span class="num">7</span>
            		<img src="<?= get_template_directory_uri().'/images/bg/clip/clip14y.png';?>" alt="">
	                <span>Бесплатная доставка и подъем в квартиру</span>
            	</li>
            	<li>
            		<span class="num">8</span>
            		<img src="<?= get_template_directory_uri().'/images/bg/clip/clip12y.png';?>" alt="">
	                <span>Бесплатный надежный монтаж</span>
            	</li>
            </ul>
        </div>
	</div>