<div class="container">
	<div class="row">
    	<h3><span>ОТЗЫВЫ НАШИХ КЛИЕНТОВ</span></h3>
        <div class="slider">
            <div class="item">                        
                <div class="review-img">
                 	<div class="block" data-toggle="modal" data-target="#image">
                 		<img src="<?= get_template_directory_uri().'/images/otzivy/otziv4.jpg';?>" class="full">
                 	</div>
                 	<div class="block" data-toggle="modal" data-target="#image">
                 		<img src="<?= get_template_directory_uri().'/images/otzivy/otziv5.jpg';?>" class="full">
                 	</div>
                 	<div class="block" data-toggle="modal" data-target="#image">
                 		<img src="<?= get_template_directory_uri().'/images/otzivy/otziv6.jpg';?>" class="full">
                 	</div>
                </div>       
                <div class="rating"><img src="<?= get_template_directory_uri().'/images/rating5.png';?>" alt=""></div>
                <div class="review-content">
                    <p>Приехал очень милый и знающий молодой человек. Сделал замеры, обговорили возможные варианты и подписали договор. Получилось действительно здорово!</p>
                </div>
                <div class="subscribe">
                    <p>СВЕТЛАНА</p>
                    <p>отзыв с сайта otzovik.com </p>
                </div>
            </div>
            <div class="item">
                <div class="review-img">
                	<div class="block" data-toggle="modal" data-target="#image">
                		<img src="<?= get_template_directory_uri().'/images/otzivy/otziv3.jpg';?>" class="full"> 
                	</div>
                </div>       
                <div class="rating"><img src="<?= get_template_directory_uri().'/images/rating5.png';?>" alt=""></div>
                <div class="review-content">
                    <p>Хотелось и того и этого, плюс холодильник большой поставить, и всё это за вменяемую стоимость. В конечном итоге остановились на Кухнев и не ошиблись: всё вместили, что хотели. Спасибо специалистам Кухнев - приложили и руки, и голову, преобразили наши 6 кв.м. </p>
                </div>
                <div class="subscribe">
                    <p>Валерия</p>
                    <p>отзыв с сайта gmstar.ru </p>
                </div>
            </div>
            <div class="item">
                <div class="review-img">
                	<div class="block" data-toggle="modal" data-target="#image">
                		<img src="<?= get_template_directory_uri().'/images/otzivy/otziv7.jpg';?>" class="full">
                	</div>
                	<div class="block" data-toggle="modal" data-target="#image">
                		<img src="<?= get_template_directory_uri().'/images/otzivy/otziv8.jpg';?>" class="full">
                	</div>
                </div>       
                <div class="rating"><img src="<?= get_template_directory_uri().'/images/rating5.png';?>" alt=""></div>
                <div class="review-content">
                    <p>Вчера поставили. Отличная кухня и отличная задумка, у меня вышло по размерам 1 в 1 как фото с сайта. Прямо такая же, только у меня дома. Цена всех устроила, даже моего мужа, который всегда удивлялся, как такая вещь, как кухня, может стоить больше 50 тысяч! </p>
                </div>
                <div class="subscribe">
                    <p>Наталья</p>
                    <p>отзыв с сайта gmstar.ru </p>
                </div>
            </div> 
            <div class="item">
                <div class="review-img">
                 	<div class="block" data-toggle="modal" data-target="#image">
                 		<img src="<?= get_template_directory_uri().'/images/otzivy/otziv1.jpg';?>" class="full">
                 	</div>
                </div>       
                <div class="rating"><img src="<?= get_template_directory_uri().'/images/rating5.png';?>" alt=""></div>
                <div class="review-content">
                    <p>После приезда дизайнера-конструктора Ивана мы решились заказать кухню. Были приятно удивлены тем, что все работы по доставке и сборке кухни были проведены в срок, и кухня была полностью укомплектована. Спасибо Игорю, Ивану и Виталию за высокий уровень сервиса! </p>
                </div>
                <div class="subscribe">
                    <p>Оксана</p>
                    <p>отзыв с сайта otzovik.com </p>
                </div>
            </div>
            <div class="item">
                <div class="review-img">
                	<div class="block" data-toggle="modal" data-target="#image">
                		<img src="<?= get_template_directory_uri().'/images/otzivy/otziv2.jpg';?>" class="full">
                	</div>
                </div>       
                <div class="rating"><img src="<?= get_template_directory_uri().'/images/rating5.png';?>" alt=""></div>
                <div class="review-content">
                    <p>Только что закончили сборку, осталась довольна. Сижу смотрю на свою кухню, и радуюсь, что а) сэкономила б) собрали без сучка и задоринки в) технику подключили бесплатно, заказывала у них. Всем рекомендую. Живу в Балашихе, так что с нашими дорогами выездной дизайнер оказался весьма кстати. </p>
                </div>
                <div class="subscribe">
                    <p>Ольга</p>
                    <p>отзыв с сайта otzovik.com </p>
                </div>  
        	</div>
    	</div>
	</div>
</div>