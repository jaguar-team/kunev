<div class="modal fade" id="kitchen" tabindex="-1" role="dialog" aria-labelledby="kitchenLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>            
            <div class="inner">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="call" tabindex="-1" role="dialog" aria-labelledby="callLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="pop-up-form">
                <h4><span>отправьте заявку</span><br>и мы вам перезвоним</h4>
                <form> 
                    <input type="text" name="name" placeholder="Ваше имя" class="name untouched">                                
                    <input type="text" name="phone" placeholder="Ваш телефон" class="phone untouched">    
                       
                    <textarea name="comment" class="comment" placeholder="Сообщение"></textarea>
                    <input type="submit" name="submit-button" class="submit-button disabled" value="Отправить">                    
                </form>
            </div>
		</div>
	</div>
</div>


<div class="modal fade" id="designer" tabindex="-1" role="dialog" aria-labelledby="designerLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="pop-up-form">
                <h4><span>отправьте заявку</span><br>на выезд дизайнера-конструктора</h4>
                <form> 
                    <input type="text" name="name" placeholder="Ваше имя" class="name untouched">                                
                    <input type="text" name="phone" placeholder="Ваш телефон" class="phone untouched">

                    <textarea name="comment" class="comment" placeholder="Сообщение"></textarea>
                    <input type="submit" name="submit-button" class="submit-button disabled" value="Отправить" id="mainsubmit">           
                </form>
                <h5 class=""><span>БЕСПЛАТНЫЙ ВЫЕЗД</span><br> дизайнера-конструктора</h5>
                <p class="text-center">Замер помещения, Демонстрация фасадов, <br> Консультация по материалам, <br>Составление дизайн-проекта, Расчёт стоимости, <br> Оформление договора, Приём предоплаты <br> <br> Стоимость выезда - БЕСПЛАТНО до 5км <br>за МКАД, далее - 30 руб./км.</p>
            </div>			

		</div>
	</div>
</div>


<div class="modal fade" id="photo" tabindex="-1" role="dialog" aria-labelledby="photoLabel">
    <div class="modal-dialog old" role="document">
        <div class="modal-content old">
            
            <div class="pop-up-form">
                <h4>ЗАГРУЗИТЕ ФОТО ИЛИ НАБРОСОК КУХНИ</h4>
                <form action="" method="POST" enctype="multipart/form-data"> 
                    <input type="text" name="name" placeholder="Ваше имя" class="name untouched">                                
                    <input type="text" name="phone" placeholder="Ваш телефон" class="phone untouched">
                    <textarea name="message" class="comment" placeholder="Сообщение"></textarea>
                    <p>Прикрепить файл</p>  
                    <input type="file" name="file" placeholder="Ваше фото">
                    <br>    
                    <input type="submit" name="submit-button-photo" class="submit-button disabled" value="Отправить" id="mainsubmit">           
                </form>
                <h5 class=""></h5>
            </div>          

        </div>
    </div>
</div>

<div class="modal fade" id="photos" tabindex="-1" role="dialog" aria-labelledby="photosLabel">
    <div class="modal-dialog old" role="document">
        <div class="modal-content old">
            
            <div class="pop-up-form">
                <h4>ОТПРАВИТЬ ЭСКИЗ НА РАСЧЁТ</h4>
                <form action="" method="POST" enctype="multipart/form-data"> 
                    <input type="text" name="name" placeholder="Ваше имя" class="name untouched">                                
                    <input type="text" name="phone" placeholder="Ваш телефон" class="phone untouched" required="required">
                    <textarea name="message" class="comment" placeholder="Ваше сообщение"></textarea>         
                    <p>Прикрепить файл</p>           
                    <input type="file" name="file_1" placeholder="Ваше фото">                    
                    <input type="file" name="file_2" placeholder="Ваше фото"> 
                    <br>                   
                    <input type="submit" name="submit-button-eskiz" class="submit-button disabled" value="Отправить" id="mainsubmit">           
                </form>
                <h5 class="">проект составляется в течении 1 дня</h5>
            </div>          

        </div>
    </div>
</div>

<div class="modal fade" id="free" tabindex="-1" role="dialog" aria-labelledby="freeLabel">
    <div class="modal-dialog old" role="document">
        <div class="modal-content old">
            
            <div class="pop-up-form">
                <h4>получить бесплатный 3D-проект</h4>
                <form action="" method="POST" enctype="multipart/form-data"> 
                    <input type="text" name="name" placeholder="Ваше имя" class="name untouched">                                
                    <input type="text" name="phone" placeholder="Ваш телефон" class="phone untouched" required="required">
                    <textarea name="message" class="comment" placeholder="Сообщение"></textarea>
                    <p>Прикрепить файл</p>           
                    <input type="file" name="file_1" placeholder="Ваше фото">                    
                    <input type="file" name="file_2" placeholder="Ваше фото"> 
                    <br>                                   
                    
                    <input type="submit" name="submit-button-project" class="submit-button disabled" value="Отправить" id="mainsubmit">           
                </form>
                <h5 class="">проект составляется в течении 1 дня</h5>
            </div>          

        </div>
    </div>
</div>

<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="successLabel">
	<div class="modal-dialog" role="document">
        <div class="modal-content">    		
            <p>Ваша заявка отправлена<br>
                Наш менеджер свяжется с вами</p>
            <div class="button" data-dismiss="modal" aria-label="Close">
                <p>Ок</p>
            </div>                
        </div>
	</div>
</div>


<div class="modal fade" id="image" tabindex="-1" role="dialog" aria-labelledby="imageLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content img">
            
        </div>
    </div>
</div>


<div class="button-group">
    <button class="btn" data-toggle="modal" data-target="#photos">Эскиз на расчёт</button>      
    <button class="btn" data-toggle="modal" data-target="#free">Бесплатный проект</button>
	<button class="btn" data-toggle="modal" data-target="#designer">Вызвать дизайнера</button>
	<button class="btn" data-toggle="modal" data-target="#call">Получить консультацию</button>
          
</div>
