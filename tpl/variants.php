<div class="container">
	<div class="row">
		<h3>Варианты планировок</h3>
		<div class="content">
			<div class="block">
				<h4>Линейная</h4>
				<div class="text-center">
					<img src="<?= get_template_directory_uri().'/images/plan/plan1.png';?>" alt="">
				</div>					
			</div>
			<div class="block">
				<h4>Островная</h4>
				<div class="text-center">
					<img src="<?= get_template_directory_uri().'/images/plan/plan2.png';?>" alt="">
				</div>					
			</div>
			<div class="block">
				<h4>L - образная</h4>
				<div class="text-center">
					<img src="<?= get_template_directory_uri().'/images/plan/plan3.png';?>" alt="">
				</div>					
			</div>
			<div class="block">
				<h4>U - образная</h4>
				<div class="text-center">
					<img src="<?= get_template_directory_uri().'/images/plan/plan4.png';?>" alt="">
				</div>					
			</div>
			<div class="block">
				<h4>Двухлинейная</h4>
				<div class="text-center">
					<img src="<?= get_template_directory_uri().'/images/plan/plan5.png';?>" alt="">
				</div>					
			</div>
			<div class="block">
				<h4>G - образная</h4>
				<div class="text-center">
					<img src="<?= get_template_directory_uri().'/images/plan/plan6.png';?>" alt="">
				</div>					
			</div>				
		</div>
		<h5>НЕ ЗНАЕТЕ, КАКАЯ ПЛАНИРОВКА ПОДХОДИТ ВАМ?</h5>
		<button class="btn" data-toggle="modal" data-target="#call">Заказать обратный звонок</button>
	</div>
</div>