<div class="container">
	<h3><span>Наши кухни</span></h3>
	<div class="catalog">
		<div class="row">
			<ul class="nav nav-tabs" role="tablist">
				<?php 
								$args = array(
						'taxonomy' => 'product-cat',
						'order' => 'ask',
						'hide_empty' => false,
					);
					$kuhni = get_terms( $args );
					$i = 0;
					foreach ($kuhni as $value):?>
			    <li role="presentation" class="<?= ($i==0) ? 'active' : ''; ?>"><a href="#<?=$value->slug?>" aria-controls="<?=$value->slug?>" role="<?=$value->slug?>" data-toggle="tab">
			    	<?=$value->description?><br>
			    	<span><?=$value->name?></span>
			    </a></li>
			     <?php $i++; endforeach; ?>
			  </ul>


			  <!-- Tab panes -->
			<div class="tab-content">
			  	<?php $i = 0;
			  		  foreach ( $kuhni as $value ):?>
			    <div role="tabpanel" class="tab-pane <?= ($i==0) ? 'active' : ''; ?>" id="<?=$value->slug?>">
			    	<div id="carousel<?=$value->slug ?>" class="carousel slide" data-ride="carousel" data-wrap="false" data-interval="false">
						<div class="carousel-inner" role="listbox">
			    			<?php $single_kuhnya = query_posts(array('product-cat' => $value->slug, 'post_type' => 'products','numberposts' => -1));
			      			$n = count($single_kuhnya)/4;
			      			$n = intval($n);
			      			for($j = 0; $j <= $n; $j++):?>
			      			<?php if(!empty($single_kuhnya[$j*4])):?>					    	
					    	<div class="item <?= ($j==0) ? 'active' : ''; ?>">
					    		<div class="block-container">
					    			<?php for($k = $j*4; $k < $j*4+4; $k++):?>
					    			<?php if(!empty($single_kuhnya[$k])):?>	
						    		<div class="block" data-toggle="modal" data-target="#kitchen" data-slider="<?= $value->slug ?>" data-number="<?= $k ?>">		    		
							    		<div class="label">
							    			<p class="old-price"><?= get_post_meta($single_kuhnya[$k]->ID, 'coast', 1); ?> <span class="fa fa-rub"></span></p>
							    			<p class="new-price"><?= get_post_meta($single_kuhnya[$k]->ID, 'new_coast', 1); ?> <span class="fa fa-rub"></span></p>
							    		</div>
							    		<div class="info">
							    			<h3><?=$single_kuhnya[$k]->post_title?></h3>
							    			<ul>
							    				<li>
							    					<span class="title">Фасад</span>
							    					<span><?= get_post_meta($single_kuhnya[$k]->ID, 'fasad', 1); ?></span>
							    				</li>
							    				<li>
							    					<span class="title">Каркас</span>
							    					<span><?= get_post_meta($single_kuhnya[$k]->ID, 'karkas', 1); ?></span>
							    				</li>
							    				<li>
							    					<span class="title">Столешница</span>
							    					<span><?= get_post_meta($single_kuhnya[$k]->ID, 'desk', 1); ?></span>
							    				</li>
							    				<li>
							    					<span class="title">Фурнитура</span>
							    					<span><?= get_post_meta($single_kuhnya[$k]->ID, 'furnitura', 1); ?></span>
							    				</li>
							    			</ul>
							    			<button class="btn" data-toggle="modal" data-target="#call">Получить консультацию</button>
											<button class="btn" data-toggle="modal" data-target="#free">Бесплатный проект</button>
							    			<p class="small-text">*Цена указана за метр погонный в комплектации "Стандарт".</p>
							    		</div>
							    		<div class="img">
							    			<?=get_the_post_thumbnail($single_kuhnya[$k]->ID,'full')?>
							    		</div>
						    		</div>
						    		<?php endif;?>
						    		<?php endfor; ?>	
						    	</div>
						    </div>	
						<?php endif; ?>
						<?php endfor; ?>
			    		</div>
			    		<a class="slider-arrows next" href="#carousel<?=$value->slug?>" role="button" data-slide="next">
						    
						</a>

						<a class="slider-arrows prev" href="#carousel<?=$value->slug?>" role="button" data-slide="prev">
						    
						</a>
			    	</div>	
			    </div>
			    <?php $i++; endforeach; ?>
			</div>
		</div>
	</div>
</div>