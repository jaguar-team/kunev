<div class="container">
	<div class="content max hidden-sm hidden-xs">
		<div class="row">
            <div class="block">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip25r.png';?>" alt="">
                <h3>Широкий ценовой<br>
                    диапазон на любой<br>
                    бюджет</h3>
            </div>
            <div class="block">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip26r.png';?>" alt="">
                <h3>Прямые поставки с<br>
                    собственного производства</h3>
            </div>
            <div class="block">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip27r.png';?>" alt="">
                <h3>Скидки и социальные льготы<br>
                    для всех наших клиентов
                </h3>
            </div>
            <div class="block">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip28r.png';?>" alt="">
                <h3>Мы не тратимся <br>
                    на аренду<br>
                    больших магазинов</h3>
            </div>
            <div class="center-block">
                <h2>Наши цены-<br>
                    <span>ниже<br>
                        салонных!</span></h2>
            </div>
        </div>
	</div>
	<div class="content min visible-sm visible-xs">
		<div class="row">
			<div class="center-block col-sm-12 col-xs-12">
                    <h2>Наши цены-<br>
                    <span>ниже<br>
                    салонных!</span></h2>
            </div>
			<div class="block col-sm-12 col-xs-12">
                    <img src="<?= get_template_directory_uri().'/images/bg/clip/clip25r.png';?>" alt="">
                    <h3>Широкий ценовой<br>
                        диапазон на любой<br>
                        бюджет</h3>
            </div>
            <div class="block col-sm-12 col-xs-12">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip26r.png';?>" alt="">
                <h3>Прямые поставки с<br>
                    собственного производства</h3>
            </div>
            <div class="block col-sm-12 col-xs-12">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip27r.png';?>" alt="">
                <h3>Скидки и социальные льготы<br>
                    для всех наших клиентов
                </h3>
            </div>
            <div class="block col-sm-12 col-xs-12">
                <img src="<?= get_template_directory_uri().'/images/bg/clip/clip28r.png';?>" alt="">
                <h3>Мы не тратимся <br>
                    на аренду<br>
                    больших магазинов</h3>
            </div>
        </div>    
	</div>
</div>