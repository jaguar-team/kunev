<?php
/**
 * Author: Vlad Dneprov, https://www.linkedin.com/in/vladislav-dneprov
 * Author URI: http://jaguar-team.com
 * Date: 11.11.2016
 * Version: 1.0.0
 */

// load_theme_textdomain( 'gr', get_template_directory() . '/languages' );
function my_login_logo(){
   echo '
   <style type="text/css">
        #login h1 a { background: url('. get_bloginfo('template_directory') .'/images/logo-wp.png) no-repeat 0 0 !important; width: 326px; height: 67px; }
    </style>';
}
add_action('login_head', 'my_login_logo');
require(__DIR__.'/core/custom-post.php');
require(__DIR__.'/core/banner.php');
require(__DIR__.'/core/ajax.php');
require(__DIR__.'/core/modal-file.php');
add_action( 'admin_menu', 'kuhnev_remove_menu_items' );
 
function kuhnev_remove_menu_items() {
    // тут мы укахываем ярлык пункты который удаляем.
    remove_menu_page('edit.php?post_type=page'); // Удаляем пункт "Комментарии"
    remove_menu_page( 'edit.php' );
    remove_menu_page('edit-comments.php');
    }


/** add javascript **/
add_action( 'wp_enqueue_scripts', 'add_scripts' );
function add_scripts() {
	
	/** lib **/
	wp_enqueue_script( 'jquery' );
	
	wp_enqueue_script( 'yandex', 				'http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU', array( 'jquery' ), '', true );
	wp_enqueue_script( 'map', 					get_template_directory_uri().'/js/map.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'bootstrap', 			get_template_directory_uri().'/js/bootstrap.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'swipe', 				get_template_directory_uri().'/js/bcSwipe.min.js', array( 'jquery' ), '', true );
	//wp_enqueue_script( 'comagic', 				'http://app.comagic.ru/static/cs.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'slick', 				get_template_directory_uri().'/js/slick/slick.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'inputmask', 			get_template_directory_uri().'/js/inputmask/jquey.inputmask.bundle.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'main', 					get_template_directory_uri().'/js/main.js', array( 'jquery' ), '', true );
	wp_localize_script( 'main', 'send_mail_kunev', array('url' => admin_url('admin-ajax.php')) );
	wp_localize_script( 'main', 'get_kitchen', array('url' => admin_url('admin-ajax.php')) );
	
	
    wp_enqueue_media();
	/** custom **/
	

}

/** add css **/
add_action( 'wp_print_styles', 'add_styles' );
function add_styles() {

	wp_localize_script('main', 'myajax',
		array(
			'url' => admin_url('admin-ajax.php')
		)
	);

	
	/** fonts **/
	wp_enqueue_style( 'font-awesome', 		get_template_directory_uri().'/css/font-awesome.min.css' );

	/** libs **/
	wp_enqueue_style( 'bootstrap', 			get_template_directory_uri().'/css/bootstrap.min.css' );
	wp_enqueue_style( 'slick', 				get_template_directory_uri().'/js/slick/slick.css' );
	wp_enqueue_style( 'slick-theme', 		get_template_directory_uri().'/js/slick/slick-theme.css' );

	/** custom **/	
	wp_enqueue_style( 'animate', 			get_template_directory_uri().'/css/animate.css' );
	wp_enqueue_style( 'main', 				get_template_directory_uri().'/style.css' );
	
}

add_theme_support( 'post-thumbnails' ); # add thubnails

/** registration nav-menu **/
register_nav_menus(array(
	'top' 			=> 'Main menu',
	'footer' 		=> 'Footer',
));

function add_more_buttons($buttons) {
 $buttons[] = 'fontsizeselect';
 $buttons[] = 'fontselect';
 $buttons[] = 'styleselect';
 $buttons[] = 'del';
 $buttons[] = 'sup';
 $buttons[] = 'sub';
 $buttons[] = 'hr';
 return $buttons;
 }
 add_filter("mce_buttons_3", "add_more_buttons");
 if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
    function wpex_mce_text_sizes( $initArray ){
        $initArray['fontsize_formats'] = "40px 50px 60px 70px 80px 90px 100px 110px 120px 130px 140px 150px";
        return $initArray;
    }
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );


