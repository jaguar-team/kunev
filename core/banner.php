<?php

add_action('admin_menu', 'register_my_custom_submenu_page');

function register_my_custom_submenu_page() {
	add_menu_page( 'Настройки темы', 'Настройки темы', 'manage_options', 'my_theme_options', 'options_print' );
	add_submenu_page( 'my_theme_options', 'Баннер', 'Баннер', 'manage_options', 'banner', 'banner_print' );
	add_submenu_page( 'my_theme_options', 'Блок уточнить цену', 'Блок уточнить цену', 'manage_options', 'block_cost', 'block_cost_print' );
	
}

function options_print() {
	wp_enqueue_media();?>
	<div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>

		<form action="options.php" method="POST">
			<?php
				settings_fields( 'main-setting' );     // скрытые защитные поля
				do_settings_sections( 'Настройки темы' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
			?>
		</form>
		<?php $img = get_option('img_background'); ?>
		<h2>Картинка для фона</h2>
		<form action="" method="POST">
			<?php if($img): ?>
				<img src="<?=$img?>" width="550px" height="300px">
			<?php endif; ?>
			<?php wp_nonce_field( 'my_file_upload', 'fileup_nonce' ); ?>
			<p>	
				<button class="add_img_background button button-primary button-large">Выбрать файл</button>
				<input type="hidden" name="imgbackground">
				<input class="button button-primary button-large" type="submit" name="save" value="Загрузить файл" />
			</p>
		</form>
		<script>
			jQuery(document).ready(function($){

				$('.add_img_background').live('click', function(e) {
					var selector = jQuery(this);
					e.preventDefault();
					var image = wp.media({
					title: 'Загрузить изображения',
					multiple: false
					}).open().on('select', function(e){
						// This will return the selected image from the Media Uploader, the result is an object
						var uploaded_image = image.state().get('selection').first().toJSON().url;
						// We convert uploaded_image to a JSON object to make accessing it easier
						// Let's assign the url value to the input field

						selector.parent().parent().find('input[type="hidden"]').val(uploaded_image);
					});
				});
			});
 		</script>
	</div>
	<?php
}

add_action('admin_init', 'main_setting');

function main_setting() {
	register_setting('main-setting', 'main_option'); //регистрация главных настроек
	add_settings_section('section_info', 'Главные настройки', '', 'Настройки темы');//добавление секции для главных настроек
	add_settings_field('email','Адрес эл. почты', 'fill_email','Настройки темы', 'section_info');
	
}

add_action('admin_init', 'banner_setting');

function banner_setting() {
	register_setting('banner-setting', 'banner_option'); //регистрация главных настроек
	add_settings_section('section_info_banner', 'Настройки баннера', '', 'Баннер'); 
	add_settings_field('header_top','Верхняя надпись в баннере', 'fill_banner','Баннер', 'section_info_banner',array('name' => 'header_top', 'number' => 'wpeditor1',));
	add_settings_field('header_center','Центральная надпись в баннере', 'fill_banner','Баннер', 'section_info_banner',array('name' => 'header_center', 'number' => 'wpeditor2',));
	add_settings_field('header_bot','Нижняя надпись в баннере', 'fill_banner','Баннер', 'section_info_banner',array('name' => 'header_bot', 'number' => 'wpeditor3',));
}

function fill_email() {
	$val = get_option('main_option');
	$val = $val['email'];
	?>

	<label>
		<input type="text" name="main_option[email]" size="100px" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Введите имейл. Если хотите 2 имейла то введите через запятую</p>
	</label>
	<?php
}

function fill_banner($arg) {

	$option = get_option('banner_option');
	$name = $arg['name'];

	wp_editor( $option[$name], 'wpeditor'.$name, array(
		'wpautop' => 0,
		'media_buttons'     => false,
		'textarea_name'     => 'banner_option['.$name.']',
		'textarea_rows'     => '10',
	) );
}

add_action('admin_init', 'block_cost_setting');
function block_cost_setting() {
	register_setting('block-cost-setting', 'block_cost_option');
	add_settings_section('section_info_block_cost', 'Настройки Блока уточнить ценну', '', 'Блок уточнить цену');
	add_settings_field('name_block','Название блока', 'fill_banner_block_cost','Блок уточнить цену', 'section_info_block_cost',array('name' => 'name_block', 'number' => 'wpeditor1',));
	add_settings_field('header_top','Верхняя надпись в баннере', 'fill_banner_block_cost','Блок уточнить цену', 'section_info_block_cost',array('name' => 'header_top_block', 'number' => 'wpeditor2',));
	add_settings_field('header_bot','Нижняя надпись в баннере', 'fill_banner_block_cost','Блок уточнить цену', 'section_info_block_cost',array('name' => 'header_bot_block', 'number' => 'wpeditor3',));


}

function fill_banner_block_cost($arg) {

	$option = get_option('block_cost_option');
	$name = $arg['name'];

	wp_editor( $option[$name], 'wpeditor'.$name, array(
		'wpautop' => 0,
		'media_buttons'     => false,
		'textarea_name'     => 'block_cost_option['.$name.']',
		'textarea_rows'     => '10',
	) );
}

function block_cost_print() {

	wp_enqueue_media();
	$img = get_option('banner_img_block_cost');?> 
		<div class="wrap">
			<h2><?php echo get_admin_page_title() ?></h2>
			<form action="" method="POST">
				<?php if($img): ?>
					<img src="<?=$img?>" width="550px" height="300px">
				<?php endif; ?>
				<?php wp_nonce_field( 'my_file_upload', 'fileup_nonce' ); ?>
				<p>	
					<button class="add_img button button-primary button-large">Выбрать файл</button>
					<input type="hidden" name="imgbanner_block_cost">
					<input class="button button-primary button-large" type="submit" name="save" value="Загрузить файл" />
				</p>
			</form>
			<form action="options.php" method="POST">
				<?php
				settings_fields( 'block-cost-setting' );     // скрытые защитные поля
				do_settings_sections( 'Блок уточнить цену' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
				?>
			</form>
		</div>	

		<script>
        jQuery(document).ready(function($){

            $('.add_img').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Let's assign the url value to the input field
                        
                        selector.parent().parent().find('input[type="hidden"]').val(uploaded_image);
                    });
            });
        });
 </script>

		

<?php 
}

function banner_print() {
	wp_enqueue_media();
	$img = get_option('banner_img');?> 
		<div class="wrap">
			<h2><?php echo get_admin_page_title() ?></h2>
			<form action="" method="POST">
				<?php if($img): ?>
					<img src="<?=$img?>" width="550px" height="300px">
				<?php endif; ?>
				<?php wp_nonce_field( 'my_file_upload', 'fileup_nonce' ); ?>
				<p>	
					<button class="add_img button button-primary button-large">Выбрать файл</button>
					<input type="hidden" name="imgbanner">
					<input class="button button-primary button-large" type="submit" name="save" value="Загрузить файл" />
				</p>
			</form>
			<form action="options.php" method="POST">
				<?php
				settings_fields( 'banner-setting' );     // скрытые защитные поля
				do_settings_sections( 'Баннер' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
				?>
			</form>
		</div>	

		<script>
        jQuery(document).ready(function($){

            $('.add_img').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Let's assign the url value to the input field
                        
                        selector.parent().parent().find('input[type="hidden"]').val(uploaded_image);
                    });
            });
        });
 </script>

		

<?php 
}		

	if(isset($_POST['save']) && isset($_POST['imgbanner_block_cost'])) {
		if(!add_option('banner_img_block_cost', $_POST['imgbanner_block_cost']))
			update_option('banner_img_block_cost', $_POST['imgbanner_block_cost']);
	}

	if(isset($_POST['save']) && isset($_POST['imgbanner'])) {
		if(!add_option('banner_img', $_POST['imgbanner']))
			update_option('banner_img', $_POST['imgbanner']);
	}

	if(isset($_POST['save']) && isset($_POST['imgbackground'])) {
		if(!add_option('img_background', $_POST['imgbackground']))
			update_option('img_background', $_POST['imgbackground']);
	}


