<?php
//Добавление произвольных постов товаров
function products_init() {
    $labels = array(
        'name'                  => 'Товары',
        'singular_name'         => 'Товар',
        'menu_name'             => 'Товары',
        'name_admin_bar'        => 'Товар',
        'add_new'               => 'Добавить товар',
        'add_new_item'          => 'Добавить новый товар',
        'new_item'              => 'Новый товар',
        'edit_item'             => 'Редактировать товар',
        'all_items'             => 'Все товары',
        'search_items'          => 'Найти товар',
        'not_found'             => 'Товар не найден',
        'not_found_in_trash'    => 'Товаров нет в корзине',
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 4,
        'menu_icon'             => 'dashicons-cart',
        'supports'              => array( 'title', 'thumbnail' ),
    );

    register_post_type( 'products' , $args);
}
add_action( 'init', 'products_init' );

//Добавление таксономии категории к товарам
function create_taxonomy(){
    // заголовки
    $labels = array(
        'name'              => 'Категории товара',
        'singular_name'     => 'Категория товара',
        'search_items'      => 'Найти категории',
        'all_items'         => 'Все категории',
        'parent_item'       => 'Категория',
        'parent_item_colon' => 'Категория:',
        'edit_item'         => 'Редактировать данные категории товара',
        'update_item'       => 'Обновить данные категории товара',
        'add_new_item'      => 'Добавить новую категорию товара',
        'new_item_name'     => 'Новая категория товара',
        'menu_name'         => 'Категория',
    );
    // параметры
    $args = array(
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => $labels,
        'description'           => '', // описание таксономии
        'public'                => true,
        'publicly_queryable'    => null, // равен аргументу public
        'show_in_nav_menus'     => true, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        'hierarchical'          => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin'              => false,
        'show_in_quick_edit'    => null, // по умолчанию значение show_ui
    );
    register_taxonomy('product-cat', array('products'), $args );
}
add_action('init', 'create_taxonomy');

add_action('add_meta_boxes', 'my_extra_fields', 1);

//Добавление произвольных полей к товарам и фасадам
function my_extra_fields() {
    add_meta_box( 'extra_fields', 'Информация о товаре', 'fields_products', 'products', 'normal', 'high'  );
    add_meta_box( 'extra_fields', 'Информация о фасаде', 'fields_fasad', 'fasad', 'normal', 'high'  );
}

//Добавление произвольных полей к товарам
function fields_products($post) {
    ?>
    <p>
        <label>Стоимость начальная</label>
        <input type="text" name="products[coast]" value="<?= get_post_meta($post->ID, 'coast', 1); ?>"/>
    </p>
    <p>
        <label>Стоимость со скидкой</label>
        <input type="text" name="products[new_coast]" value="<?= get_post_meta($post->ID, 'new_coast', 1); ?>"/>
    </p>
    <p>
        <label>Фасад</label>
        <input type="text" name="products[fasad]" size="100" value="<?= get_post_meta($post->ID, 'fasad', 1); ?>"/>
    </p>
    <p>
        <label>Каркас</label>
        <input type="text" name="products[karkas]" size="100" value="<?= get_post_meta($post->ID, 'karkas', 1); ?>"/>
    </p>
    <p>
        <label>Столешница</label>
        <input type="text" name="products[desk]" size="100" value="<?= get_post_meta($post->ID, 'desk', 1); ?>"/>
    </p>
    <p>
        <label>Фурнитура</label>
        <input type="text" name="products[furnitura]" size="100" value="<?= get_post_meta($post->ID, 'furnitura', 1); ?>"/>
    </p>
    <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

    <input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

    <?php
}
//Сохранение произвольных полей товаров
add_action('save_post', 'update_fields_products', 0);
/* Сохраняем данные, при сохранении поста */
function update_fields_products($post_id ) {

    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
    if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

    if( !isset($_POST['products']) ) return false;

    // Все ОК! Теперь, нужно сохранить/удалить данные
    // $_POST['credit_company'] = array_map('trim', $_POST['extra']);
    foreach( $_POST['products'] as $key=>$value ){
        if( empty($value) ){
            delete_post_meta($post_id, $key); // удаляем поле если значение пустое
            continue;
        }

        update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
    }
    return $post_id;
}
//Добавление характеристик к категориям фасадов
add_action("fasad-cat_edit_form_fields", 'add_new_custom_fields_fasad_cat');
function add_new_custom_fields_fasad_cat($term){
    wp_enqueue_media();
    $harak = json_decode(get_term_meta( $term->term_id, 'product_harak', 1 ));
    ?>

   
    <p>
    <table id="table_harak" style="text-align: center; vertical-align: middle;">
        <h2>Характеристики <button id="add_harak" class="button button-primary">Добавить новую</button></h2>
        <tr>
            <th>Описание</th>
            <th>Фото</th>
            <th>Действия</th>
        </tr>
        <?php if ($harak): ?>
                <?php foreach ($harak as $key => $hara): ?>
                <tr>
                    <td><input type="text" value="<?= $hara->name; ?>" name="product_harak_name[]" /></td>
                    <td><img src="<?= $hara->value; ?>"></td>
                    <td style="width: 120px;"><button class="button button-primary delete_harak">Удалить</button></td>
                    <td><input type="hidden" value="<?= $hara->value; ?>" name="product_harak_value[]"></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
    </p>
    <script>
        jQuery(document).ready(function($){

            $('.add_img').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Let's assign the url value to the input field
                        
                        selector.parent().parent().find('input[type="hidden"]').val(uploaded_image);
                    });
            });

            $('.delete_harak').click(function (e_delete) {
                e_delete.preventDefault();
                $(this).parent().parent().remove();
            });

            $('#add_harak').click(function(e){
                var count = $('#table_harak tr').length;
                e.preventDefault();
                $('#table_harak').append(`
                    <tr>
                        <td><input type="text" name="product_harak_name[]" /></td>
                        <td><button class="add_img">Выбрать файл</button></td>
                        <td><button class="button button-primary delete_harak">Удалить</button></td>
                        <td><input type="hidden" name="product_harak_value[]"></td>
                    </tr>
                `);
            });
        });
    </script>


<?php 
}

//Обновление произвольных полей категории фасадов
function save_custom_taxonomy_meta($term_id){

    
    $extra = array_map('trim', $_POST['fasad-cat']);
    
              if ( isset($_POST['product_harak_name']) && isset($_POST['product_harak_value']) ) {

        $data = array();

        foreach ($_POST['product_harak_name'] as $key => $value) {
            $data[$key + 1] = array('name' => $_POST['product_harak_name'][$key], 'value' => $_POST['product_harak_value'][$key]);
        }

        $value = json_encode($data, JSON_UNESCAPED_UNICODE);
        update_term_meta($term_id, 'product_harak', $value);
    }

    foreach( $extra as $key => $value ){
        if( empty($value) ){
            delete_term_meta( $term_id, $key ); // удаляем поле если значение пустое
            continue;
        }

        update_term_meta( $term_id, $key, $value ); // add_term_meta() работает автоматически
    }   

    return $term_id;
}
add_action("edited_fasad-cat", 'save_custom_taxonomy_meta');


//Добавление произвольных постов фасадов
function fasad_init() {
    $labels = array(
        'name'                  => 'Фасады',
        'singular_name'         => 'Фасад',
        'menu_name'             => 'Фасады',
        'name_admin_bar'        => 'Фасад',
        'add_new'               => 'Добавить фасад',
        'add_new_item'          => 'Добавить новый фасад',
        'new_item'              => 'Новый фасад',
        'edit_item'             => 'Редактировать фасад',
        'all_items'             => 'Все фасады',
        'search_items'          => 'Найти фасад',
        'not_found'             => 'Фасад не найден',
        'not_found_in_trash'    => 'Фасадов нет в корзине',
    );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'has_archive'           => false,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-admin-multisite',
        'supports'              => array( 'title', 'thumbnail' ),
    );

    register_post_type( 'fasad' , $args);
}
add_action( 'init', 'fasad_init' );

//Добавление таксаномии категории к фасадамы
function create_taxonomy_fasad(){
    // заголовки
    $labels = array(
        'name'              => 'Категории фасада',
        'singular_name'     => 'Категория фасада',
        'search_items'      => 'Найти категории',
        'all_items'         => 'Все категории',
        'parent_item'       => 'Категория',
        'parent_item_colon' => 'Категория:',
        'edit_item'         => 'Редактировать данные категории фасада',
        'update_item'       => 'Обновить данные категории фасада',
        'add_new_item'      => 'Добавить новую категорию фасада',
        'new_item_name'     => 'Новая категория фасада',
        'menu_name'         => 'Категория',
    );
    // параметры
    $args = array(
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => $labels,
        'description'           => '', // описание таксономии
        'public'                => true,
        'publicly_queryable'    => null, // равен аргументу public
        'show_in_nav_menus'     => true, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        'hierarchical'          => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin'              => false,
        'show_in_quick_edit'    => null, // по умолчанию значение show_ui
    );
    register_taxonomy('fasad-cat', array('fasad'), $args );
}
add_action('init', 'create_taxonomy_fasad');


//Добавление произвольных полей фасадов
function fields_fasad($post) {
    
        $images = json_decode(get_post_meta( $post->ID, 'images', 1 ));

    ?>

    <?php if (isset($images)): ?>
        <?php foreach ($images as $key => $images): ?>
            <img style="height: 100px; width:100px;" src="<?= $images; ?>" alt="" />
        <?php endforeach; ?>
    <?php endif; ?>  
    <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

   

    <?php
}
//Добавление галереи к фасадам
function fasad_images_extra_fields() {
    add_meta_box(
        'side_images_product',
        'Изображения фасада',
        'fasad_side_images_fields',
        'fasad',
        'side',
        'low'
    );
}
add_action( 'add_meta_boxes', 'fasad_images_extra_fields', 1 );
//Функция добавления галереи к фасадам
function fasad_side_images_fields($post) {
    wp_enqueue_media(); ?>


    <p>
        <button class="button button-primary add_images">Добавить изображения</button>
        <input class="input_images" type="hidden" name="fasad[images]" value="">
        </p>
    <script>
        jQuery(document).ready(function(){

            jQuery('.add_images').click(function(e) {
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: true
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first();
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Output to the console uploaded_image
                        var array_object = image.state().get('selection').toJSON();

                        var image_url_array = [];
                        if(array_object.length > 7) {

                            var image_urls = '';
                        }
                        else {
                            for(var i = 0; i < array_object.length; i++) {
                                image_url_array.push(array_object[i].url);
                            }

                            var image_urls = JSON.stringify(image_url_array);
                        }
                        // Let's assign the url value to the input field
                        jQuery('.input_images').val(image_urls);
                    });
            });
        });
    </script>

    <?php
}
//Сохранение фасадов
add_action('save_post', 'update_fields_fasad', 0);
/* Сохраняем данные, при сохранении поста */
function update_fields_fasad($post_id ) {

    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
    if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

    if( !isset($_POST['fasad']) ) return false;

   
    foreach( $_POST['fasad'] as $key=>$value ){
        if( empty($value) && $key != 'images' ){
            delete_post_meta($post_id, $key); // удаляем поле если значение пустое
            continue;
        }
        if($key == 'images' && !empty($value)){
        update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
            
     }
        if($key !='images')
            update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
    }
    return $post_id;
}
//Добавление произвольных колонок к товарам
function product_custom_column($defaults) {

    $columns = array(
        'img'           => '<span class="dashicons dashicons-format-image"></span>',        
        'title'         => 'Название',
        'coast'         => 'Цена',
        'new_coast'     => 'Цена со скидкой',
        'category'      => 'Категория',
        'date'          => 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_products_posts_columns', 'product_custom_column' );

/*
 * Get value to column fields
 */
//Получение произвольных колонок к товарам
function product_get_column_value($column, $post_id) {
    if( $column == 'img' )
        if (get_the_post_thumbnail( $post_id, array( 60, 60 ) ))
            echo get_the_post_thumbnail( $post_id, array( 60, 60 ) );
        else
            echo 'Изображение не задано.';

    if( $column == 'coast' )
        if (get_post_meta( $post_id, 'coast', 1 ))
            echo number_format(get_post_meta( $post_id, 'coast', 1 ), 2, '.', ' ').' &#x584;';
        else
            echo 'Цена не задана.';
    if( $column == 'new_coast' )
        if (get_post_meta( $post_id, 'new_coast', 1 ))
            echo number_format(get_post_meta( $post_id, 'new_coast', 1 ), 2, '.', ' ').' &#x584;';
        else
            echo 'Цена не задана.';
    
    if( $column == 'category') {
       $category = get_the_terms( $post_id, 'product-cat');
        foreach ($category as $value) {
            echo $value->description.' '.$value->name.',';
        }
    }
 
    
}
add_action( 'manage_products_posts_custom_column', 'product_get_column_value', 10, 2 );


add_action('admin_head', 'add_views_column_css');
function add_views_column_css(){
    echo '<style type="text/css">.column-coast{width:10%;}</style>';
}

// добавляем возможность сортировать колонку
add_filter('manage_edit-products_sortable_columns', 'add_views_sortable_column');
function add_views_sortable_column($sortable_columns){
    $sortable_columns['coast'] = 'coast_coast';
     $sortable_columns['new_coast'] = 'new_coast_new_coast';
    return $sortable_columns;
}

// изменяем запрос при сортировке колонки
add_filter('pre_get_posts', 'add_column_views_request');
function add_column_views_request( $object ){
    if( $object->get('orderby') != 'coast_coast' && $object->get('orderby') != 'new_coast_new_coast')
        return;

    $object->set('meta_key', 'coast');
    $object->set('orderby', 'meta_value_num');
    $object->set('meta_key', 'new_coast');
    $object->set('orderby', 'meta_value_num');
}

// Функция отображения фультра в админке категорий для продуктов
function true_taxonomy_filter_products() {
    global $typenow; // тип поста
    if( $typenow == 'products' ){ // для каких типов постов отображать
        $taxes = array('product-cat'); // таксономии через запятую
        foreach ($taxes as $tax) {
            $current_tax = isset( $_GET[$tax] ) ? $_GET[$tax] : '';
            $tax_obj = get_taxonomy($tax);
            $tax_name = mb_strtolower($tax_obj->labels->name);
            // функция mb_strtolower переводит в нижний регистр
            // она может не работать на некоторых хостингах, если что, убирайте её отсюда
            $terms = get_terms($tax);
            if(count($terms) > 0) {
                echo "<select name='$tax' id='$tax' class='postform'>";
                echo "<option value=''>Все $tax_name</option>";
                foreach ($terms as $term) {
                    echo '<option value='. $term->slug, $current_tax == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
                }
                echo "</select>";
            }
        }
    }
}
 
add_action( 'restrict_manage_posts', 'true_taxonomy_filter_products' );
// Функция добавления колонок в админке для фасадов
function product_custom_column_fasad($defaults) {

    $columns = array(      
        'title'         => 'Название',
        'category'      => 'Категория',
        'date'          => 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_fasad_posts_columns', 'product_custom_column_fasad' );
// Функция получения колонок в админке для фасадов
function product_get_column_value_fasad($column, $post_id) {    
    if( $column == 'category') {
       $category = get_the_terms( $post_id, 'fasad-cat');
        foreach ($category as $value) {
            echo $value->description.' '.$value->name.'<br>';
        }
    }
 
    
}
add_action( 'manage_fasad_posts_custom_column', 'product_get_column_value_fasad', 10, 2 );
// Функция отображения фультра в админке категорий для продуктов
function true_taxonomy_filter_fasad() {
    global $typenow; // тип поста
    if( $typenow == 'fasad' ){ // для каких типов постов отображать
        $taxes = array('fasad-cat'); // таксономии через запятую
        foreach ($taxes as $tax) {
            $current_tax = isset( $_GET[$tax] ) ? $_GET[$tax] : '';
            $tax_obj = get_taxonomy($tax);
            $tax_name = mb_strtolower($tax_obj->labels->name);
            // функция mb_strtolower переводит в нижний регистр
            // она может не работать на некоторых хостингах, если что, убирайте её отсюда
            $terms = get_terms($tax);
            if(count($terms) > 0) {
                echo "<select name='$tax' id='$tax' class='postform'>";
                echo "<option value=''>Все $tax_name</option>";
                foreach ($terms as $term) {
                    echo '<option value='. $term->slug, $current_tax == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
                }
                echo "</select>";
            }
        }
    }
}
 
add_action( 'restrict_manage_posts', 'true_taxonomy_filter_fasad' );