<?php

function sendUploadPhotoBlock() {
	if (isset($_POST['submit-button-photo'])) {
		if ($_POST['submit-button-photo']) {

			$attachment = array();

			/** save file **/
			if (isset($_FILES['file'])) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				$overrides = array( 'test_form' => false );
				$file = &$_FILES['file'];
				$upload = wp_handle_upload( $file, $overrides );

				if (!isset($upload['error'])) {
					$attachment  = array($upload['file']);
				}
			}

			$data  = array(
				'name' 		=> (isset($_POST['name']) ? $_POST['name'] : 'Отсутствует'),
				'phone' 	=> (isset($_POST['phone']) ? $_POST['phone'] : 'Отсутствует'),
				'message' 	=> (isset($_POST['message']) ? $_POST['message'] : 'Отсутствует'),
				'name_form' => 'ЗАГРУЗИТЕ ФОТОГРАФИЮ ЛЮБОЙ КУХНИ',
			);

			/** send email **/
			$headers = array();
			$mail 		= get_option('main_option');
			$to 		= explode(",", $mail['email']);
			$subject 	= 'Загрузка фото';
			//$headers[] = 'From: <callback@кухнев.ру>';
			$headers[] 	= 'content-type: text/html';
			$message = getEmailMessage($data);
			if (wp_mail( $to, $subject, $message, $headers, $attachment)) {
				sendComagin($data);
				return true;
			} else {
				return false;
			}
		}
	}
}

function sendFreeProject() {

	if (isset($_POST['submit-button-project'])) {
		if ($_POST['submit-button-project']) {
			$attachment = array();

			/** save file **/
			if (isset($_FILES['file_1'])) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				$overrides = array( 'test_form' => false );
				$file = &$_FILES['file_1'];
				$upload = wp_handle_upload( $file, $overrides );

				if (!isset($upload['error'])) {
					$attachment[]  = $upload['file'];
				}
			}
			
			if (isset($_FILES['file_2'])) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				$overrides = array( 'test_form' => false );
				$file = &$_FILES['file_2'];
				$upload = wp_handle_upload( $file, $overrides );

				if (!isset($upload['error'])) {
					$attachment[]  = $upload['file'];
				}
			}
			
			$data  = array(
				'name' 		=> (isset($_POST['name']) ? $_POST['name'] : 'Отсутствует'),
				'phone' 	=> (isset($_POST['phone']) ? $_POST['phone'] : 'Отсутствует'),
				'message' 	=> (isset($_POST['message']) ? $_POST['message'] : 'Отсутствует'),
				'name_form' => 'БЕСПЛАТНЫЙ ПРОЭКТ',
			);

			/** send email **/
			$headers = array();
			$mail 		= get_option('main_option');
			$to 		= explode(",", $mail['email']);
			$subject 	= 'Загрузка фото';
			//$headers[] = 'From: <callback@кухнев.ру>';
			$headers[] 	= 'content-type: text/html';
			$message = getEmailMessage($data);
			if (wp_mail( $to, $subject, $message, $headers, $attachment)) {
				sendComagin($data);
				return true;
			} else {
				return false;
			}
		}
	}

}

function sendEskiz() {
	if (isset($_POST['submit-button-eskiz'])) {
		if ($_POST['submit-button-eskiz']) {

			$attachment = array();

			/** save file **/
			if (isset($_FILES['file_1'])) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				$overrides = array( 'test_form' => false );
				$file = &$_FILES['file_1'];
				$upload = wp_handle_upload( $file, $overrides );

				if (!isset($upload['error'])) {
					$attachment[]  = $upload['file'];
				}
			}

			if (isset($_FILES['file_2'])) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				$overrides = array( 'test_form' => false );
				$file = &$_FILES['file_2'];
				$upload = wp_handle_upload( $file, $overrides );

				if (!isset($upload['error'])) {
					$attachment[]  = $upload['file'];
				}
			}
				
			$data  = array(
				'name' 		=> (isset($_POST['name']) ? $_POST['name'] : 'Отсутствует'),
				'phone' 	=> (isset($_POST['phone']) ? $_POST['phone'] : 'Отсутствует'),
				'message' 	=> (isset($_POST['message']) ? $_POST['message'] : 'Отсутствует'),
				'name_form' => 'ЭСКИЗ НА ПРОСЧЕТ',
			);

			/** send email **/
			$headers = array();
			$mail 		= get_option('main_option');
			$to 		= explode(",", $mail['email']);
			$subject 	= 'Загрузка фото';
			//$headers[] = 'From: <callback@кухнев.ру>';
			$headers[] 	= 'content-type: text/html';
			$message = getEmailMessage($data);
			if (wp_mail( $to, $subject, $message, $headers, $attachment)) {
				sendComagin($data);
				return true;
			} else {
				return false;
			}
		}
	}


}

function sendComagin($data) {
	echo '
	<script>
			jQuery(document).ready(function($){
				setTimeout(function() { 
					$("#success").modal("show"); 

					Comagic.addOfflineRequest({
						name: "'.$data['name'].'",
						phone: "'.$data['phone'].'",
						message: "'.$data['message'].'"
					}, function(res){
						//console.log(data);
						var status = JSON.parse(res.response).success;
			            if (status) {                        
			                afValidated = true;
			            }
			            else {
			                afValidated = false;
			            }
					});
				}, 2000);
			});
		</script>
	';
}

function getEmailMessage($data) {
	return '
		<div id="style_14753119890000000757_BODY">
		<center>
			<table cellpadding="8" cellspacing="0" style="width: 540px;padding: 0;width: 100% !important;background: #ffffff;margin: 0;background-color: #ffffff;" border="0">
				<tbody>
					<tr>
						<td valign="top">
							<table cellpadding="0" cellspacing="0" style="border-radius: 4px;-webkit-border-radius: 4px;border: 1px #dceaf5 solid;-moz-border-radius: 4px;" border="0" align="center">
								<tbody>
									<tr><td colspan="3" height="6"></td></tr>
									<tr>
										<td>
											<table cellpadding="0" cellspacing="0" style="line-height: 25px;" border="0" align="center">
												<tbody>
													<tr><td colspan="3" height="30"></td></tr>
													<tr>
														<td width="36"></td>
														<td width="454" align="left" style="color: #444444;border-collapse: collapse;font-size: 11pt;font-family: proxima_nova, \'Open Sans\', \'Lucida Grande\', \'Segoe UI\', Arial, Verdana, \'Lucida Sans Unicode\', Tahoma, \'Sans Serif\';max-width: 454px;" valign="top">
														<h4>Заявка на офрмление заказа '.get_bloginfo('name').'</h4>
														Данные пользователя:<br>
														Имя: '.$data['name'].';<br>
														Телефон: '.$data['phone'].'.<br><br>
														Текст заказа от '.$data['name'].':<br>
														'.$data['message'].'<br><br>
														<p style="color:#909090;font-size:80%">*Этот E-mail был отправлен через форму "'.$data['name_form'].''.get_bloginfo('name').'</p>
														</td>
														<td width="36"></td>
													</tr>
													<tr><td colspan="3" height="36"></td></tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
							<table cellpadding="0" cellspacing="0" align="center" border="0">
								<tbody>
									<tr><td height="10"></td></tr>
									<tr>
										<td style="padding: 0;border-collapse: collapse;">
											<table cellpadding="0" cellspacing="0" align="center" border="0">
												<tbody>
													<tr style="color: #a8b9c6;font-size: 11px;font-family: proxima_nova, \'Open Sans\', \'Lucida Grande\', \'Segoe UI\', Arial, Verdana, \'Lucida Sans Unicode\', Tahoma, \'Sans Serif\';-webkit-text-size-adjust: none;">
														<td width="400" align="left"></td>
														<td width="128" align="right">©'.get_bloginfo('name').'</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</center>
	</div>
	';
}