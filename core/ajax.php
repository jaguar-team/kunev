<?php

add_filter( 'wp_mail_from_name', 'vortal_wp_mail_from_name' );
function vortal_wp_mail_from_name( $email_from )

{
	return 'Кухнев.рф';
}
	
/** send form to email  **/
function my_action_send_mail() {

	$headers = array();

	$mail 		= get_option('main_option');
	$to 		= explode(",", $mail['email']);
	$subject 	= $_POST['subject'];
	$headers[] = 'From: <callback@кухнев.ру>';
	$headers[] 	= 'content-type: text/html';
	$message = '
		<div id="style_14753119890000000757_BODY">
	<center>
		<table cellpadding="8" cellspacing="0" style="width: 540px;padding: 0;width: 100% !important;background: #ffffff;margin: 0;background-color: #ffffff;" border="0">
			<tbody>
				<tr>
					<td valign="top">
						<table cellpadding="0" cellspacing="0" style="border-radius: 4px;-webkit-border-radius: 4px;border: 1px #dceaf5 solid;-moz-border-radius: 4px;" border="0" align="center">
							<tbody>
								<tr><td colspan="3" height="6"></td></tr>
								<tr>
									<td>
										<table cellpadding="0" cellspacing="0" style="line-height: 25px;" border="0" align="center">
											<tbody>
												<tr><td colspan="3" height="30"></td></tr>
												<tr>
													<td width="36"></td>
													<td width="454" align="left" style="color: #444444;border-collapse: collapse;font-size: 11pt;font-family: proxima_nova, \'Open Sans\', \'Lucida Grande\', \'Segoe UI\', Arial, Verdana, \'Lucida Sans Unicode\', Tahoma, \'Sans Serif\';max-width: 454px;" valign="top">
													<h4>Заявка на офрмление заказа '.get_bloginfo('name').'</h4>
													Данные пользователя:<br>
													Имя: '.$_POST['name'].';<br>
													Телефон: '.$_POST['phone_number'].'.<br><br>
													Текст заказа от '.$_POST['name'].':<br>
													'.$_POST['message'].'<br><br>
													<p style="color:#909090;font-size:80%">*Этот E-mail был отправлен через форму '.$_POST['subject'].' на сайте '.get_bloginfo('name').'</p>
													</td>
													<td width="36"></td>
												</tr>
												<tr><td colspan="3" height="36"></td></tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" align="center" border="0">
							<tbody>
								<tr><td height="10"></td></tr>
								<tr>
									<td style="padding: 0;border-collapse: collapse;">
										<table cellpadding="0" cellspacing="0" align="center" border="0">
											<tbody>
												<tr style="color: #a8b9c6;font-size: 11px;font-family: proxima_nova, \'Open Sans\', \'Lucida Grande\', \'Segoe UI\', Arial, Verdana, \'Lucida Sans Unicode\', Tahoma, \'Sans Serif\';-webkit-text-size-adjust: none;">
													<td width="400" align="left"></td>
													<td width="128" align="right">©'.get_bloginfo('name').'</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</center>
</div>
	'
	;


	wp_mail( $to, $subject, $message, $headers );
	wp_die();
};
add_action('wp_ajax_send_mail', 'my_action_send_mail');
add_action('wp_ajax_nopriv_send_mail', 'my_action_send_mail');

function my_action_get_kitchen_by_slug() {
	header('Content-Type: application/json');
	$category_slug = $_POST['slug'];
	$kitchen = query_posts(array('product-cat' => $category_slug, 'post_type' => 'products','numberposts' => -1));
	$i = 0;
	$response = array();
	foreach ($kitchen as $value) {
		$response[$i]['title'] = $value->post_title;
		$response[$i]['coast'] = get_post_meta($value->ID, 'coast', 1);
		$response[$i]['new_coast'] = get_post_meta($value->ID, 'new_coast', 1);
		$response[$i]['fasad'] = get_post_meta($value->ID, 'fasad', 1);
		$response[$i]['karkas'] = get_post_meta($value->ID, 'karkas', 1);
		$response[$i]['desk'] = get_post_meta($value->ID, 'desk', 1);
		$response[$i]['furnitura'] = get_post_meta($value->ID, 'furnitura', 1);
		$response[$i]['img'] = wp_get_attachment_image_src(get_post_thumbnail_id($value->ID),'full', true);
		$i++;
	}
	echo json_encode($response);
	wp_die();
}

add_action('wp_ajax_get_kitchen_by_slug', 'my_action_get_kitchen_by_slug');
add_action('wp_ajax_nopriv_get_kitchen_by_slug', 'my_action_get_kitchen_by_slug');



function my_action_send_mail_files() {
	wp_send_json($_FILES['sdf']);
	wp_die();
}

add_action('wp_ajax_send_mail_files', 'my_action_send_mail_files');
add_action('wp_ajax_nopriv_send_mail_files', 'my_action_send_mail_files');
